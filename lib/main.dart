import 'package:flutter/material.dart';

import 'screens/grocery_list_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const UShoppingListApp());
}

class UShoppingListApp extends StatelessWidget {
  const UShoppingListApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'U Shopping List App',
      theme: ThemeData.dark().copyWith(
        colorScheme: ColorScheme.fromSeed(
          seedColor: const Color.fromARGB(255, 147, 229, 250),
          brightness: Brightness.dark,
          surface: const Color.fromARGB(255, 42, 51, 59),
        ),
        scaffoldBackgroundColor: const Color.fromARGB(255, 50, 58, 60),
      ),
      home: const GroceryListScreen(),
    );
  }
}
