import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:u_shopping_list_app/data/data_categories.dart';

import '../models/category.dart';
import '../models/grocery_item.dart';
import '../screens/new_item_screen.dart';

const String URL_DB_REAL = 'u-shopping-list-app-default-rtdb.firebaseio.com';
const String URL_DB_FAKE = 'fake-url.firebaseio.com';
const int DELETE_ITEM_DELAY = 3;

class GroceryListScreen extends StatefulWidget {
  const GroceryListScreen({super.key});

  @override
  State<GroceryListScreen> createState() => _GroceryListScreenState();
}

class _GroceryListScreenState extends State<GroceryListScreen> {
  List<GroceryItem> _groceryItems = [];
  bool _isLoading = true;
  String _errorMsg = '';
  String _errorExtended = '';

  @override
  void initState() {
    super.initState();
    _loadItems();
  }

  void _loadItems() async {
    final url = Uri.https(URL_DB_REAL, 'shopping-list.json');
    late final response;

    try {
      response = await http.get(url);

      if (response.body == 'null' || response.body == '') {
        setState(() {
          _isLoading = false;
        });
        return;
      }

      /// Expected type from json.decode(response.body):
      /// Map<String, Map<String, dynamic>>,
      ///
      /// but it's causing problem with current converter's type:
      /// _Map<String, dynamic>
      ///
      /// That's why using expecting type here
      /// is Map<String, dynamic>.
      final Map<String, dynamic> itemsData = json.decode(response.body);
      final List<GroceryItem> _loadedItems = [];

      for (final item in itemsData.entries) {
        final Category category = dataCategories.values.firstWhere(
          (dataCategory) => dataCategory.name == item.value['category'],
        );

        _loadedItems.add(
          GroceryItem(
            id: item.key,
            name: item.value['name'],
            quantity: item.value['quantity'],
            category: category,
          ),
        );
      }

      setState(() {
        _groceryItems = _loadedItems;
        _isLoading = false;
      });
    } catch (error) {
      setState(() {
        _errorMsg = 'Smthng went wrong. \nPlease, try again later.';

        _errorExtended =
            '\n=------= =------= =------= \nServer was returned with status: ${response.statusCode}. \nExtended error info: $error \n=------= =------= =------=';
      });
    }
  }

  void _addItem() async {
    final newItem = await Navigator.of(context).push<GroceryItem>(
      MaterialPageRoute(
        builder: (ctx) => const NewItemScreen(),
      ),
    );

    if (newItem == null) {
      return;
    }

    setState(() {
      _groceryItems.add(newItem);
      _isLoading = false;
    });
  }

  void _onRemoveItem(GroceryItem item) async {
    final url = Uri.https(URL_DB_REAL, 'shopping-list/${item.id}.json');
    final int groceryIndex = _groceryItems.indexOf(item);

    setState(() => _groceryItems.remove(item));

    try {
      final testRequest = await http.get(url);

      if (testRequest.statusCode >= 400) {
        /// Test Error.
        /// Just an Instance of Error() for example;
        throw Error();
      }

      final itemDeleteTimer = Timer(
        const Duration(seconds: DELETE_ITEM_DELAY),
        () => http.delete(url),
      );

      if (!context.mounted) {
        return;
      }

      ScaffoldMessenger.of(context).clearSnackBars();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: const Duration(seconds: DELETE_ITEM_DELAY),
          content: const Text('Grocery was removed. Undo?'),
          action: SnackBarAction(
            label: 'Undo',
            onPressed: () {
              itemDeleteTimer.cancel();
              setState(() => _groceryItems.insert(groceryIndex, item));
            },
          ),
        ),
      );
    } catch (_) {
      setState(() => _groceryItems.insert(groceryIndex, item));

      if (!context.mounted) {
        return;
      }

      ScaffoldMessenger.of(context).clearSnackBars();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          duration: Duration(seconds: DELETE_ITEM_DELAY),
          content: Text('Something went wrong... Please, try again later.'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget content = const Center(
      child: Text(
        'No items added yet...',
        style: TextStyle(fontSize: 24.0),
      ),
    );

    if (_isLoading) {
      content = const Center(child: CircularProgressIndicator());
    }

    if (_groceryItems.isNotEmpty) {
      content = ListView.builder(
        itemCount: _groceryItems.length,
        itemBuilder: (ctx, index) {
          final GroceryItem currentGrocery = _groceryItems[index];

          return Dismissible(
            key: ValueKey(currentGrocery.id),
            onDismissed: (_) => _onRemoveItem(currentGrocery),
            background: Container(
              color: Theme.of(context).colorScheme.error.withOpacity(0.75),
            ),
            child: ListTile(
              leading: Container(
                width: 30.0,
                height: 30.0,
                color: currentGrocery.category.color,
              ),
              title: Text(
                currentGrocery.name,
                style: const TextStyle(fontSize: 20.0),
              ),
              trailing: Text(
                currentGrocery.quantity.toString(),
                style: const TextStyle(fontSize: 20.0),
              ),
            ),
          );
        },
      );
    }

    if (_errorMsg.isNotEmpty) {
      content = Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                _errorMsg.trim(),
                style: const TextStyle(fontSize: 24.0),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 24.0),
              Text(
                _errorExtended,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Groceries'),
        actions: [
          IconButton(
            onPressed: _addItem,
            icon: const Icon(Icons.add),
          ),
        ],
      ),
      body: content,
    );
  }
}
