import '../models/category.dart';
import '../models/grocery_item.dart';
import 'data_categories.dart';

final dataGroceryItems = [
  GroceryItem(
      id: 'a',
      name: 'Milk',
      quantity: 1,
      category: dataCategories[Categories.dairy]!),
  GroceryItem(
      id: 'b',
      name: 'Bananas',
      quantity: 5,
      category: dataCategories[Categories.fruit]!),
  GroceryItem(
      id: 'c',
      name: 'Beef Steak',
      quantity: 1,
      category: dataCategories[Categories.meat]!),
];
